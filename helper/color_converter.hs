-- |

module Main where

import Numeric (readHex)

import System.IO


main :: IO ()
main = do
  hSetBuffering stdin LineBuffering
  loop

loop :: IO ()
loop = do
  line <- getLine
  let parsed = parseLine line
  case parsed of
    Nothing -> putStrLn "Incorrect format. Expecting hex format \"#000000\"."
    Just tuple -> putStrLn $ show $ map (\a -> a/255) tuple

  loop

parseLine :: String -> Maybe [Double]
parseLine input =
  case input of
    ('#':rest) -> parseHex rest
    _ -> Nothing

parseHex :: String -> Maybe [Double]
parseHex [] = Just []
parseHex (_:[]) = Nothing
parseHex (a:b:rest) = case (parseHex rest) of
  Nothing -> Nothing
  Just parsed -> let [(num, _)] = readHex (a:b:[])
                 in Just (num:parsed)
