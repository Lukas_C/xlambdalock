module Main where

import XLambdaLock.Saver.Main
import XLambdaLock.Saver.ImageSaver

main :: IO ()
main = xlambdasaver $ imageSaverHook
