## xlambdalock
![Screenshot](https://gitlab.com/Lukas_C/images/-/raw/master/xlambdalock.png)

`xlambdalock` is an extensible locker for the X Window System based on [`xsecurelock`](https://github.com/google/xsecurelock) and the Haskell programming language.
It aims to provide a framework for customizing the visual appearance through use of bindings to the [`cairo`](https://www.cairographics.org) C library, provided by [`gtk2hs`](https://github.com/gtk2hs/gtk2hs).
While the `gtk2hs` library is mainly focused on GTK applications, `xlambdalock` itself is written based only on the `Xlib` interface.

**This piece of software is at the moment still in early development. Expect stuff to not work properly and configs to change.**

### Contents
[[_TOC_]]

## Installation
### Arch Linux
~~A PKGBUILD is provided on a separate branch.~~
This configuration is no longer supported due to the difficulties of integrating Haskell programs properly with the Arch packaging system.
I may try to integrate this again in the future, but right now it's just not worth the hassle.


## Building
The build process relies on the ["Haskell Tool Stack"](https://docs.haskellstack.org/en/stable/README/) and should feel familiar to the [build process of XMonad](https://github.com/xmonad/xmonad/blob/master/INSTALL.md).

### Source
First download the sources into an appropriate location:
``` sh
cd /path/to/install
git clone https://gitlab.com/Lukas_C/xlambdalock
```

### Config
Create the desired config directory:
``` sh
mkdir -p ~/.config/xlambdalock && cd ~/.config/xlambdalock
```

Next, create a `stack.yaml` file with the following contents:
``` yaml
resolver: lts-20.15

extra-deps:
- co-log-0.5.0.0
- chronos-1.1.5
- typerep-map-0.5.0.0

packages:
- /path/to/install/xlambdalock  # <- change this accordingly
```
~~Newer versions of `gcc` cannot be used due to a change in the syntax of the preprocessor that will cause the [`gtk2hs` compilation to fail](https://github.com/haskell/c2hs/issues/268).~~

Create your initial `xlambdalock.hs` config file:
``` haskell
import XLambdaLock

main :: IO ()
main = xlambdalock $ def
```
This will create a base config with some sensible defaults set.
Customization and configuration are discussed further down.

Lastly, create a `build` file:
``` sh
#!/bin/sh
origin=$(pwd)
target=$HOME/.local/bin
if [ -z "${XDG_CACHE_HOME}" ];then
    cache=$HOME/.cache/xlambdalock
else
    cache="${XDG_CACHE_HOME}/xlambdalock"
fi

mkdir -p $cache/
cp stack.yaml $cache/
cd $cache

exec stack ghc          \
  --package xlambdalock \
  --                    \
  --make "$origin/xlambdalock.hs" \
  -i                    \
  -ilib                 \
  -fforce-recomp        \
  -main-is main         \
  -v0                   \
  -outputdir "$cache"   \
  -o "$target/auth_xlambdalock"
```
The copying of files keeps temporary files away from the config directory due to a [long standing bug](https://github.com/commercialhaskell/stack/issues/1731) not allowing absolute paths for the `.stack-work` folder.
If you don't care about this, you can just use the `stack` command on its own.

Make it executable:
``` sh
chmod +x build
```

You can now try and see if it compiles:
``` sh
./build
```
The resolver proposed by this README in the `stack.yaml` should usually work fine.
If an error is thrown that suggests adding a package to `extra-deps`, do it and try again.
If a lot of those errors crop up, you might want to [try a different resolver](https://docs.haskellstack.org/en/stable/GUIDE/#resolvers-and-changing-your-compiler-version).

### Optional steps
To achieve the blurred background, [`ImageMagick`](https://imagemagick.org/index.php)'s `convert` tool is used by a custom `saver_xlambdalock`.
A `xlambdalock-converter` that takes ordinary hex `#rrggbb` or `#rrggbbaa` values and converts them to tuples of values in the range of `0` to `1` is also included.

To build the saver and converter, do:
``` sh
cd /path/to/install/xlambdalock
stack build
```
The directory of the compiled binaries will be noted in the output log. Copy them to a suitable location.
Alternatively you can copy them directly to your user local binary directory using:
```
stack install
```
The configuration of the saver is discussed in the configuration section.

### Documentation
Documentation is provided through the [Haddock](https://www.haskell.org/haddock/) documentation tool.
An HTML based doc can be compiled with
``` sh
cd /path/to/install/xlambdalock
stack haddock
```


## Configuration
First of all, for the impatient, here is an example `xlambdalock.hs` that gives you the same look as demonstrated in the thumbnail of this README:
``` haskell
import XLambdaLock
import XLambdaLock.Defaults.ImageSaver

main :: IO ()
main = xlambdalock $ def { environment = [("XSECURELOCK_GLOBAL_SAVER", "/path/to/saver_xlambdalock")]
                         , initHook = blurInitHook
                         }
```
We use the base `def` config, which provides some default hooks and replace the `environment` and `initHook` fields.
Overriding other functions works the same way.
With that out of the way, we can go more in depth on some more details.

### Architecture
Similar to the configuration process of the XMonad window manager, the main function `xlambdalock` uses a `Config` data type that holds variables and hooks that are executed by the process at particular points:
``` haskell
data Config = Config { environment :: [(String, String)]
                     , initHook    :: InitHook
                     , setupHook   :: SetupHook
                     , loopHook    :: LoopHook
                     , eventHook   :: EventHook
                     }
```

The following figure demonstrates the architecture of the locker and which hooks are executed where.
```
                 ┌───────────┐   ┌────────┐
  ┌─env not set─►│environment├──►│initHook│    ┌────────┐
  │              └───────────┘   └─┬──────┘  ┌►│loopHook├────┐
┌─┴───┐                            │         │ └────────┘    │
│Start│     ┌───exec xsecurelock───┘     ┌──►│               │
└─┬───┘     │                            │   │ ┌───────────┐ │
  │         ▼  ┌─────────┐               │   └─┤EventLambda│◄┘
  └─env set───►│setupHook├───────────────┘     └───────────┘
               └─────────┘                           ▲
                                       ┌─────────┐   │
                                       │eventHook├───┘
                                       └─────────┘
```
([asciiflow](https://asciiflow.com/))

When the process is started, it checks if the `XSCREENSAVER_WINDOW` environment variable is set.
If the process is started by the user, the variable should not be set and we set the environment variables contained in `environment`.
We then execute the `initHook`.
The type signatures of the individual hooks are discussed further down.
After replacing ourselves through an `exec`, `xsecurelock` will set the `XSCREENSAVER_WINDOW` variable and launch us again.
We then execute the `setupHook` and enter the render loop.
Everytime an event occurs, e.g. a key is pressed, we execute the `eventHook`.
This hook generates a closure of type `EventLambda` that gets placed into the render loop.

With this overview we can take a look at the individual hooks and some examples in form of the default hooks that are provided.

### Hooks
The hooks provide the main way to control what is displayed on the screen.
In the following we will explore the available hooks in more detail.
The default implementation for each hook can be found in the `XLambdaLock.Defaults.Defaults` module.

#### The `initHook`
This hook is primarily intended for setting up stuff that we can't do anymore when we are launched by `xsecurelock`.
An example of this would be saving a screenshot and blurring it, so that we can later display it.
This functionality is also already provided by the `blurInitHook` in the `XLambdaLock.Defaults.ImageSaver` module.

The type signature is as follows:
``` haskell
type InitHook = IO ()
```

As we can see, the `InitHook` type is just a regular `IO ()` function.
This also means that we can easily compose hooks, e.g. our own `customInitHook` with the `blurInitHook`:
``` haskell
def { initHook = customInitHook <> blurInitHook }
```

#### The `setupHook`
Usually it is not necessary to redraw the entire monitor when an event occurs.
Also until the first event occurs, the `eventHook` will not be called, so if we want to draw something only a single time or before anything has really happened, this hook becomes useful.

The type signature looks like:
``` haskell
type SetupHook = Surface        -- Root surface
               -> Double        -- Surface width
               -> Double        -- Surface height
               -> Render ()
```
Please refer to the cairo documentation for more information on `Surface` and the `Render` monad.

The `defaultSetupHook` simply draws the Haskell logo:
``` haskell
defaultSetupHook :: Surface -> Double -> Double -> Render ()
defaultSetupHook surf w h = paintLogoCentered surf w h $ drawHaskellLogo
```

#### The `eventHook`
This hook is called every time a new event occurs.
``` haskell
type EventHook = PwEvent -> EventLambda
```
The hook receives the event and converts it into an `EventLambda`, which will be placed on to the render loop, getting called once each loop iteration.

A `PwEvent` looks as follows:
``` haskell
-- |Represents something that might happen with a password while it is entered or processed.
data PwEvent = PwSuccess                -- Password entered was correct.
             | PwError PwError          -- Password entered was unsuccessful.
             | PwAction PwOperation Int -- An action was performed on the password.
                                        -- The Int holds the current length of the password.
             deriving (Show, Eq)

-- |Possible operations on a password.
data PwOperation = PwAppend Char        -- Append a char to the end.
                 | PwDelete             -- Remove one char from the end.
                 | PwReset              -- Clear the password field.
                 | PwCommit             -- Submit the password for checking if it is correct.
                 deriving (Show, Eq)

-- |Represents possible errors why the password attempt was not successful.
data PwError = PwFailure                -- Password entered was incorrect.
             | PwEmpty                  -- Password entered was empty.
             | PwLockout                -- Max number of attempts granted by PAM were reached.
             | PwAuthError Int          -- Something went wrong while communicating with the auth process.
                                        -- The Int holds the exit code of the process.
             deriving (Show, Eq)
```
The `EventLambda` that is returned is effectively a closure and has this type signature:
``` haskell
type EventLambda = Surface              -- Root surface
                 -> Double              -- Surface width
                 -> Double              -- Surface height
                 -> DeltaSeconds        -- Time delta since the creation of the event, in seconds
                 -> Render Bool         -- Return wether the event can be removed
```
Since a time delta since the creation of the event is provided, this can be used to create animations, e.g. fadeouts.
To provide an example of how an `EventHook` may look, here is the `defaultEventHook` from `XLambdaLock.Defaults.Defaults`:
``` haskell
defaultEventHook :: PwEvent -> EventLambda
defaultEventHook (PwError err) =
  let color = case err of
                PwLockout -> red
                _ -> orange
                where orange = (0.9647,0.4549,0.0) -- #F67400
                      red    = (1.0,0.0,0.0)
  -- here is the EventLambda for an error case
  in (\ surf w h _ -> do
         paintLogoCentered surf w h $ drawLogoLambda color 1.0
         -- remove instantly, since there is no redraw necessary
         return True)
defaultEventHook e =
  let color = case e of
                PwAction (PwAppend _) _ -> white
                PwAction PwDelete     _ -> blue
                PwAction PwReset      _ -> blue
                _                       -> white
                where
                  -- KDE breeze colorscheme
                  white = (0.9882,0.9882,0.9882) -- #FCFCFC
                  blue  = (0.2392,0.6823,0.9137) -- #3DAEE9
  -- here is the alternate EventLambda for all other cases
  in (\ surf w h delta -> do
         let alpha = max (1.0 - delta/0.2) 0  -- the fadeout time is 0.2s
         paintLogoCentered surf w h $ drawLogoLambda color alpha
         -- alpha equals to zero is equivalent to delta >= fadeout time
         return $ alpha == 0)
```
This hook redraws the "lambda" of the Haskell logo in different colors with a fade-out effect.
The color is determined by the type of event.

#### The `loopHook`
This hook has the same type signature as the `setupHook`, however it is called every time the render loop has finished an iteration and flushed the output buffer.
This can be useful if some part of the Background needs to be redrawn because multiple `EventLambda`s paint with transparency.
In this case the the transparency effects of the `EventLambda`s should combine within one cycle of the render loop,
but the stack up over multiple render loop iterations is undesired.

The `defaultLoopHook` repaints the lambda of the haskell logo in its default color with full opacity,
so that the fade out effects by the `EventLambda`s can be overlaid:
``` haskell
defaultLoopHook :: Surface -> Double -> Double -> Render ()
defaultLoopHook surf w h = paintLogoCentered surf w h $ drawLogoLambda darkGray 1.0
```

### Some basic `cairo` resources
Since this locker relies on the `cairo` library and its bindings to Haskell, here are some basic resources that illustrate how to use the library.
Alternatively, you can have a look at the `XLambdaLock.Drawing.HaskellLogo` module to see some examples of how the library was used in this project.
- Hackage documentation of `cairo-0.12.2` (very useful for details on the Haskell side): <https://hackage.haskell.org/package/cairo>
- Quick demo of the working principle of the Haskell bindings: <https://metric-space.github.io/posts/2018-03-21-haskell-cairo-tutorial.html>
- For everything else: <https://www.cairographics.org>

## Final notes
This project was inspired by the [`xringlock` project of Ma_124](https://gitlab.com/Ma_124/xringlock).
For a nice out of the box locker written in C, look over there.

Lastly, this started as a project to get a foothold in Haskell.
While this project has now had its first major rewrite, I still assume that some of the code is still not the best, since I'm still relatively new to the Language.
If you have suggestions for improving stuff, don't hesitate to send me a message.
