#!/usr/bin/env bash
set -euo pipefail

stack build

if [[ ${BASH_ARGV[0]} == "run" ]]; then
  if ! pgrep -x "Xephyr"; then
    echo "Starting Xephyr"
    Xephyr :1 -screen 600x400 &> /dev/null &
  else
    echo "Xephyr already started, proceeding"
  fi

  export DISPLAY=:1

  export DEBUG="true"
  stack exec auth_xlambdalock
fi
