-- |

module XLambdaLock.Saver.Main where

import Control.Concurrent (threadDelay)
import Control.Monad (when)

import Data.Maybe (isJust)

import System.Environment (lookupEnv)
import System.Exit

import Text.XML.HXT.DOM.Util (decimalStringToInt)

import XLambdaLock.Saver.Types

-- |The main 'xlambdasaver' function called by @saver_xlambdalock@.
xlambdasaver :: SaverHook  -- ^'SaverHook' to run
             -> IO ()
xlambdasaver saverHook = do
  lookupEnv "XSCREENSAVER_WINDOW" >>= maybe
    (die "Could not find XSCREENSAVER_WINDOW variable, exiting.")
    (\wIDstr -> do debug <- lookupEnv "DEBUG"
                   when (isJust debug) (putStrLn $ "Window ID: " ++ wIDstr)
                   -- run saverHook
                   saverHook $ fromIntegral $ decimalStringToInt wIDstr)
  -- freeze forever
  freeze
  where freeze = do threadDelay 4294967295  -- 2^32-1 microseconds
                    freeze
