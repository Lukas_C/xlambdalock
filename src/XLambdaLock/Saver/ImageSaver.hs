-- |

module XLambdaLock.Saver.ImageSaver where

import Control.Applicative (asum)
import Control.Monad.Trans.Maybe (runMaybeT, MaybeT(..))

import Data.Functor

import GHC.Float (int2Double)

import System.Directory (doesFileExist)
import System.Environment (lookupEnv)
import System.FilePath
import System.Exit

import Graphics.Rendering.Cairo hiding (x, y)
import Graphics.X11.Xlib
import Graphics.X11.Xrandr

import XLambdaLock.Graphics.Window


-- * 'SaverHook'
imageSaverHook :: XID -> IO ()
imageSaverHook wID = do
  dir <- runMaybeT getSaveDir >>= maybe
    (die "Critical error, could not find a directory to load blurred images from, exiting.")
    return

  wProps <- initWindow wID
  let dpy = wp_display wProps

  xrrGetMonitors dpy (defaultRootWindow dpy) False >>= maybe
    (die "Critical error, no monitors found, exiting.")
    (\mons -> do applyImagesToMonitors dir wProps mons
                 sync dpy False)


-- ** Helpers
applyImagesToMonitors :: FilePath          -- ^Base path where the images are located.
                      -> WProps            -- ^Properties of the window the process is allowed to draw on.
                      -> [XRRMonitorInfo]  -- ^Monitor info used to locate the appropriate image and apply it to the monitor.
                      -> IO ()
applyImagesToMonitors dir wProps = mapM_ (\mon -> do let surf = wp_surf wProps
                                                         path = getMonImagePath dir mon
                                                         -- TODO: code duplication
                                                         x = int2Double $ fromEnum $ xrr_moninf_x mon
                                                         y = int2Double $ fromEnum $ xrr_moninf_y mon
                                                     renderWith surf (drawImage path x y))


drawImage :: FilePath -> Double -> Double -> Render ()
drawImage path x y = do
  b <- liftIO $ doesFileExist path
  if b
    then (do surf <- liftIO $ imageSurfaceCreateFromPNG path
             setSourceSurface surf x y
             paint
             surfaceFinish surf
         )
    else liftIO $ putStrLn $ "Could not find image at " ++ show path ++ ", skipping."


-- * File path generation
-- |Selects a suitable location to save the blurred images in.
-- In order of preference: @XDG_RUNTIME_DIR@ (i.e. @\/run\/user\/1000\/@), @XDG_CACHE_HOME@, @~/.cache@.
-- Appends the folder @xlambdalock@ to the path.
-- Returns 'Nothing' if none of the mentioned options work.
getSaveDir :: MaybeT IO FilePath
getSaveDir = fmap (</> "xlambdalock") $ asum $ MaybeT <$> [ lookupEnv "XDG_RUNTIME_DIR"
                                                          , lookupEnv "XDG_CACHE_HOME"
                                                          , lookupEnv "HOME" <&> fmap (</> ".cache")
                                                          ]


-- |Uses the IDs obtained from 'xrr_moninf_name' as file names to create a path for saving and loading: @[loc]/[XID].png@
getMonImagePath :: FilePath -> XRRMonitorInfo -> FilePath
getMonImagePath loc mon = loc </> show (xrr_moninf_name mon) <.> "png"
