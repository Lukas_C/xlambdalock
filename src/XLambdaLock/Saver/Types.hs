-- |

module XLambdaLock.Saver.Types where

import Graphics.X11.Xlib

-- |Function that is executed one time before the saver process freezes.
-- This can be used, e.g. for displaying a static image, as the 'ImageSaver' does.
type SaverHook = XID -> IO ()
