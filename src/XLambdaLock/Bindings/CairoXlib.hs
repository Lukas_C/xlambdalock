{-# LANGUAGE ForeignFunctionInterface #-}
-- |

module XLambdaLock.Bindings.CairoXlib
  ( cairoXlibSurfaceCreate
  ) where

import Graphics.Rendering.Cairo.Types (Surface(..))
import Graphics.X11.Types (Drawable)
import Graphics.X11.Xlib.Types (Display(..), Visual(..), Dimension)

import Foreign

foreign import ccall "cairo_xlib_surface_create" cairoXlibSurfaceCreatePtr_ :: Display -> Drawable -> Visual -> Int -> Int -> IO (Ptr Surface)

-- |links a Xlib drawing surface to a display and a drawable (e.g. a window).
-- further details:
-- https://www.cairographics.org/Xlib/
-- https://www.cairographics.org/manual/cairo-XLib-Surfaces.html#cairo-xlib-surface-create
cairoXlibSurfaceCreate :: Display -> Drawable -> Visual -> Dimension -> Dimension -> IO Surface
cairoXlibSurfaceCreate dpy dw v w h = do ptr <- cairoXlibSurfaceCreatePtr_ dpy dw v (fromEnum w) (fromEnum h)
                                         foreignPtr <- newForeignPtr_ ptr
                                         return $ Surface foreignPtr
