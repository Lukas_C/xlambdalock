-- |Deals with window creation for the different processes.

module XLambdaLock.Graphics.Window
  ( WProps(..)
  , initWindow
  ) where

import GHC.Float (int2Double)
import Foreign.C.Types (CInt(..))

import Graphics.Rendering.Cairo (Surface)
import Graphics.X11.Xlib
import Graphics.X11.Xlib.Extras

import XLambdaLock.Bindings.CairoXlib


-- |Holds the important data about the instanciated X window.
data WProps = WProps { wp_display :: Display
                     , wp_screen  :: Screen
                     , wp_window  :: Window
                     , wp_pos     :: (Position, Position)
                     , wp_dims    :: (Double, Double)
                     , wp_surf    :: Surface
                     }


-- |Takes the windowID from @XSCREENSAVER_WINDOW@
-- and derives the proper window. Returns an initialized 'WProps'.
initWindow :: Window -> IO WProps
initWindow relWin = do
  dpy <- openDisplay ""
  let scr = defaultScreenOfDisplay dpy

  -- get parent window
  (_, parentWin, _) <- queryTree dpy relWin
  -- get its properties
  pWProps <- getWindowAttributes dpy parentWin

  -- getting values needed for instanciating the new window.
  -- See "man XCreateWindow" for more info on the parameters.
  let CInt x = wa_x pWProps
      CInt y = wa_y pWProps
      w = fromEnum $ wa_width pWProps
      h = fromEnum $ wa_height pWProps
      bw = 0                          -- border width
      d = defaultDepthOfScreen scr    -- bit depth
      wClass = inputOutput            -- window class
      v = defaultVisualOfScreen scr   -- GrayScale, TrueColor, etc.
      mask = cWOverrideRedirect       -- value mask

  win <- allocaSetWindowAttributes $ \attributes -> do set_override_redirect attributes True
                                                       createWindow dpy parentWin
                                                                    x y (fromIntegral w) (fromIntegral h)
                                                                    bw d wClass v
                                                                    mask attributes
  mapWindow dpy win
  surf <- cairoXlibSurfaceCreate dpy win v (fromIntegral w) (fromIntegral h)

  return WProps { wp_display = dpy
                , wp_screen  = scr
                , wp_window  = win
                , wp_pos     = (x, y)
                , wp_dims    = (int2Double w, int2Double h)
                , wp_surf    = surf
                }
