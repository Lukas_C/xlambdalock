-- |Utility module for drawing the Haskell logo.

module XLambdaLock.Drawing.HaskellLogo where

import Graphics.Rendering.Cairo

import XLambdaLock.Types

lightGray :: Color
lightGray = (0.4,0.4,0.4)

darkGray :: Color
darkGray = (0.5,0.5,0.5)

-- * Logo helpers
-- |Draws the Haskell logo (">\\=") with default colors. Dimensions are 170x120 px.
drawHaskellLogo :: Render ()
drawHaskellLogo = do
  drawLogoArrow   lightGray 1.0
  drawLogoLambda  darkGray 1.0
  drawLogoEQUpper lightGray 1.0
  drawLogoEQLower darkGray 1.0


-- * Lambda
-- |Uses 'drawLogoLambdaHelper' to draw "\\" of the logo, using 'OperatorSource'.
-- Fills area with black before applying the colored fill to reduce AA artifacts at the edges.
drawLogoLambda :: Color -> Alpha -> Render ()
drawLogoLambda (r, g, b) a = do
  setOperator OperatorSource
  drawLogoLambdaHelper
  setSourceRGBA 0 0 0 1
  fillPreserve

  setSourceRGBA r g b a
  fill

-- |Uses 'drawLogoLambdaHelper' to draw "\\" of the logo, using 'OperatorOver'.
drawLogoLambda' :: Color -> Alpha -> Render ()
drawLogoLambda' (r, g, b) a = do
  setOperator OperatorOver
  drawLogoLambdaHelper
  setSourceRGBA r g b a
  fill


-- * Other components
-- |Draws ">".
drawLogoArrow :: Color -> Alpha -> Render ()
drawLogoArrow (r, g, b) a = do
  setSourceRGBA r g b a
  moveTo  0 120
  lineTo 40  60
  lineTo  0   0
  lineTo 30   0
  lineTo 70  60
  lineTo 30 120
  closePath
  fill

-- |Draws upper bar of "=".
drawLogoEQUpper :: Color -> Alpha -> Render ()
drawLogoEQUpper (r, g, b) a = do
  setSourceRGBA r g b a
  moveTo 136.666667 85  -- TODO: clean this up a bit, infinite decimals look terrible
  lineTo 123.333333 65
  lineTo 170 65
  lineTo 170 85
  closePath
  fill

-- |Draws lower bar of "=".
drawLogoEQLower :: Color -> Alpha -> Render ()
drawLogoEQLower (r, g, b) a = do
  setSourceRGBA r g b a
  moveTo 116.666667 55  -- TODO: clean this up a bit, infinite decimals look terrible
  lineTo 102.333333 35
  lineTo 170 35
  lineTo 170 55
  closePath
  fill

-- * Helpers
-- |Traces the path of "\\". Does not fill anything.
drawLogoLambdaHelper :: Render ()
drawLogoLambdaHelper = do
  moveTo  40 120
  lineTo  80  60
  lineTo  40   0
  lineTo  70   0
  lineTo 150 120
  lineTo 120 120
  lineTo  95  82.5
  lineTo  70 120
  closePath

-- |Use a temporary surface to apply a 'Render' operation to the center of the root surface.
-- As the width and height of the temporary surface are hardcoded,
-- this function only really works for the helpers in this module.
paintLogoCentered :: Surface
                  -> Double
                  -> Double
                  -> Render ()
                  -> Render ()
paintLogoCentered surf w h f = do
  tmpSurf <- liftIO $ createSimilarSurface surf ContentColorAlpha 170 120
  liftIO $ renderWith tmpSurf f
  setSourceSurface tmpSurf (w/2.0 - 170.0/2.0) (h/2.0 - 120.0/2.0)
  paintWithAlpha 1.0
  surfaceFinish tmpSurf
  surfaceFlush surf
