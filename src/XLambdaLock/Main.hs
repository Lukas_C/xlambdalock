{-# LANGUAGE DeriveDataTypeable #-}
-- |

module XLambdaLock.Main
  ( xlambdalock
  ) where

import Paths_xlambdalock (version)

import Colog

import Control.Concurrent (forkIO)
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TChan (newTChan)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Reader

import Data.Char (ord)
import qualified Data.Text as Text
import Data.Version (showVersion)

import Graphics.X11.Types (XID)

import System.Console.CmdArgs
import System.Environment (lookupEnv, setEnv)
import System.Exit (exitSuccess)
import System.Posix.Files (readSymbolicLink)
import System.Posix.Process (executeFile)

import XLambdaLock.Loop.InputLoop
import XLambdaLock.Loop.RenderLoop
import XLambdaLock.Types


-- |The main 'xlambdalock' function. This needs to be called in main.
xlambdalock :: Config -> IO ()
xlambdalock config = do
  _ <- (cmdArgs cliConfig)
  runAppEnv (execLocker config)


-- * CLI Arguments
-- |Configuration used for the command line arguments.
data CliConfig = CliConfig {}
  deriving (Show, Data, Typeable)


-- |The default config that is updated as the command line arguments are parsed.
cliConfig :: CliConfig
cliConfig = CliConfig {} &= summary ("xlambdalock " ++ showVersion version)


-- * Exec, setup and run
-- |Checks wether the process was lauched by 'xsecurelock' or by the user.
-- Replaces itself with an instance of 'xsecurelock' if XSCREENSAVER_WINDOW is not yet set.
-- xsecurelock will then execute 'auth_xsecurelock', setting the environment variable that was not present earlier.
-- If the variable is set it will continue to running the program.
execLocker :: Config -> App ()
execLocker config = do
  -- Check if environment variable XSCREENSAVER_WINDOW is set. If not, xlambdalock has not been
  -- started through xsecurelock and we still need to start xsecurelock manually
  envWin <- liftIO $ lookupEnv "XSCREENSAVER_WINDOW"
  case envWin of
    Nothing     -> do logInfo $ "XSCREENSAVER_WINDOW variable is not set, replacing myself with xsecurelock."
                      -- place path of executable in current environment so that xsecurelock can find it
                      execPath <- liftIO $ readSymbolicLink "/proc/self/exe"
                      liftIO $ setEnv "XSECURELOCK_AUTH" execPath
                      -- set additional environment specified in the config, execute initHook
                      logInfo "Set up environment and run InitHook."
                      liftIO $ setEnvs $ environment config
                      liftIO $ initHook config
                      -- replace ourselves with xsecurelock through execvp
                      liftIO $ executeFile "xsecurelock" True [] Nothing

    Just wIDstr -> do logInfo $ "Found XSCREENSAVER_WINDOW variable \"" <> Text.pack (wIDstr) <> "\" , continuing."
                      -- convert window ID from String to XID/Word64
                      let wid = fromIntegral $ stringToInt wIDstr
                      runLoops wid config
  where
    setEnvs :: [(String, String)] -> IO ()
    setEnvs = mapM_ (uncurry setEnv)

    -- simplified version of decimalStringToInt from the hxt library
    stringToInt :: String -> Int
    stringToInt = (foldl (\ digit acc -> digit * 10 + acc) 0) . (concatMap charToInt)

    charToInt :: Char -> [Int]
    charToInt c | c >= '0' && c <= '9' = [ord c - ord '0']
                | otherwise = []



-- |Set up and run the render and input loop.
runLoops :: XID     -- ^The window ID we are allowed to draw on.
         -> Config
         -> App ()
runLoops wid config = do
  c <- liftIO $ atomically $ newTChan
  env <- App ask
  _ <- liftIO $ forkIO $ runApp (runRenderLoop c wid config) env
  runInputLoop c
  -- If we get here, 'runInputLoop' has exited successfully.
  -- TODO: Wait for 'runRenderLoop' to finish properly?

  liftIO $ exitSuccess
