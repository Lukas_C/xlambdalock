-- |Module dealing with the handling of the password verification.

module XLambdaLock.Auth.Handler
  ( ProcTuple
  , createAuthProcIO
  ) where

import Colog (logError)

import Control.Monad.IO.Class (liftIO)

import qualified Data.Text as Text (pack)

import System.Directory (doesFileExist)
import System.Environment (lookupEnv)
import System.Exit (exitWith, ExitCode(..))
import System.IO
import System.Process

import XLambdaLock.Types (App)

-- |'ProcTuple' holds the important stuff for interacting with the "Authentication Protocol Module" (APM).
-- See the README of "xsecurelock" for more details on the APM.
--
-- @(stdin, stdout, stderr, handle)@
type ProcTuple = ( Handle, Handle, Handle, ProcessHandle)

-- * Process creation
-- |Creates an APM process.
-- Returns a 'ProcTuple' containing the important ways of interacting with the process.
-- Uses the @XSECURELOCK_AUTHPROTO@ environment variable to determine the APM to use.
createAuthProcIO :: App ProcTuple
createAuthProcIO = do
  -- get location of AUTHPROTO
  maybeAuthProtoPath <- liftIO $ lookupEnv "XSECURELOCK_AUTHPROTO"
  authProtoPath <- case maybeAuthProtoPath of
                     Just path -> return path
                     -- when no custom AUTHPROTO is set, fall back to the default PAM module
                     Nothing -> do
                       exists <- liftIO $ doesFileExist fallback
                       if exists
                         then return fallback
                         else do logError $ "XSECURELOCK_AUTHPROTO not set and alternate fallback at \"" <> Text.pack fallback <> "\" not found. Exiting."
                                 liftIO $ exitWith (ExitFailure 42)
                       where
                         fallback = "/usr/lib/xsecurelock/authproto_pam"

  (Just authIn, Just authOut, Just authErr, p) <- liftIO $ createProcess (authProc authProtoPath)
  -- set line buffering to ensure the buffer is flushed after every "command" that is passed
  liftIO $ hSetBuffering authIn LineBuffering
  liftIO $ hSetBuffering authOut LineBuffering
  return (authIn, authOut, authErr, p)

  where
    authProc path = (proc path []){ std_in  = CreatePipe
                                  , std_out = CreatePipe
                                  , std_err = CreatePipe
                                  }
