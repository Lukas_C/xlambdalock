-- |

module XLambdaLock.Defaults.Defaults where

import Graphics.Rendering.Cairo

import XLambdaLock.Drawing.HaskellLogo

import XLambdaLock.Types

-- |Config with sensible defaults. Pass this to the 'xlambdalock' function.
-- Further information on the individual options is provided by the README
-- and in the documentation of 'XLambdaLock.Defaults.Config'.
def :: Config
def = Config { environment = []
             , initHook    = defaultInitHook
             , setupHook   = defaultSetupHook
             , loopHook    = defaultLoopHook
             , eventHook   = defaultEventHook
             }


-- |Default 'InitHook'.
defaultInitHook :: IO ()
defaultInitHook = return ()


-- |Default 'SetupHook'.
defaultSetupHook :: Surface -> Double -> Double -> Render ()
defaultSetupHook surf w h = paintLogoCentered surf w h $ drawHaskellLogo


-- |Default 'EventHook'.
defaultEventHook :: PwEvent -> EventLambda
defaultEventHook (PwError err) =
  let color = case err of
                PwLockout -> red
                _ -> orange
                where orange = (0.9647,0.4549,0.0) -- #F67400
                      red    = (1.0,0.0,0.0)
  in (\ surf w h _ -> do
         paintLogoCentered surf w h $ drawLogoLambda color 1.0
         -- remove instantly, since there is no redraw necessary
         return True)
defaultEventHook e =
  let color = case e of
                PwAction (PwAppend _) _ -> white
                PwAction PwDelete     _ -> blue
                PwAction PwReset      _ -> blue
                _                       -> white
                where
                  -- KDE breeze colorscheme
                  white = (0.9882,0.9882,0.9882) -- #FCFCFC
                  blue  = (0.2392,0.6823,0.9137) -- #3DAEE9
  in (\ surf w h delta -> do
         let alpha = max (1.0 - delta/0.2) 0
         paintLogoCentered surf w h $ drawLogoLambda color alpha
         -- alpha equals to zero is equivalent to delta >= fadeout time
         return $ alpha == 0)


-- |Default 'LoopHook'.
defaultLoopHook :: Surface -> Double -> Double -> Render ()
defaultLoopHook surf w h = paintLogoCentered surf w h $ drawLogoLambda darkGray 1.0
