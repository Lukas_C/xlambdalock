-- |

module XLambdaLock.Defaults.ImageSaver
  ( saveRootWindowInitHook
  , blurInitHook
  , SaveRootWindowConfig(..)
  , Process
  ) where

import Prelude hiding (FilePath)

import Control.Monad.Trans.Maybe (runMaybeT, MaybeT(..))

import System.Directory (createDirectoryIfMissing)
import System.Exit
import System.FilePath
import System.Process

import GHC.Float (int2Double)

import Graphics.Rendering.Cairo hiding (x, y)
import Graphics.X11.Xlib
import Graphics.X11.Xrandr

import XLambdaLock.Bindings.CairoXlib
import XLambdaLock.Saver.ImageSaver


-- * 'saveImageInitHook'
saveRootWindowInitHook :: SaveRootWindowConfig -> IO ()
saveRootWindowInitHook conf = runMaybeT getSaveDir >>= maybe
  (putStrLn "could not find an appropriate location to store the blurred images, skipping...")
  (\dir -> createDirectoryIfMissing False dir >> saveAndProcessRootWindow dir (process conf))

-- ** Presets
blurInitHook :: IO ()
blurInitHook = saveRootWindowInitHook SaveRootWindowConfig { process = Just (\ path -> proc "convert" [path, "-blur", "0x10", path]) }

-- ** Configuration
-- |See 'SaveRootWindowConfig'
type Process = Maybe (FilePath -> CreateProcess)

data SaveRootWindowConfig = SaveRootWindowConfig
  { process :: Process    -- ^System process to use for post-processing, if applicable.
                          -- Gets applied to every screen separately.
                          -- Receives a path to the unprocessed file, which is also the output path.
  }

-- ** Plumbing
-- |Saves the blurred root window as a png in the specified folder.
-- The window is split into the different screens according to Xrandr.
-- The file names are derived from the respective internal IDs of the screen (see 'startBlurProcesses').
saveAndProcessRootWindow :: FilePath                -- ^Folder to save in. Does not check wether the folder exists.
                         -> Process
                         -> IO ()
saveAndProcessRootWindow dir pr = do
  dpy <- openDisplay ""
  let root = defaultRootWindow dpy
      v = defaultVisualOfScreen $ defaultScreenOfDisplay dpy

  -- create a cairo surface from the root window.
  -- The resulting surface has the contents of the window.
  (_, _, _, rootWidth, rootHeight, _, _) <- getGeometry dpy root
  surf <- cairoXlibSurfaceCreate dpy root v rootWidth rootHeight
  xrrGetMonitors dpy root False >>= maybe
    (putStrLn "could not get monitor information")
    (mapM_ (saveAndProcessMonitor surf dir pr))

  surfaceFinish surf
  closeDisplay dpy


-- |Takes a list of XRRMonitorInfo structs representing a monitor and saves a blurred screenshot for each monitor.
-- Uses 'startBlurProcesses' and 'waitForBlurProcesses'.
saveAndProcessMonitor :: Surface                -- ^surface of the X root window
                      -> FilePath               -- ^Folder to save in. Does not check wether the folder exists.
                      -> Process
                      -> XRRMonitorInfo
                      -> IO ExitCode            -- ^Exit code of the process. Returns 'ExitSuccess' if process was 'Nothing'.
saveAndProcessMonitor rootSurf dir pr mon = do
  let path = getMonImagePath dir mon
  saveMonitor rootSurf mon path
  maybe
    (return ExitSuccess)
    (\ p -> do (_, _, _, h) <- createProcess $ p path
               waitForProcess h)
    pr


-- |Saves a single monitor (unblurred).
saveMonitor :: Surface          -- ^The root window surface.
            -> XRRMonitorInfo   -- ^Monitor to save.
            -> FilePath         -- ^Path (including file name) to save to.
            -> IO ()
saveMonitor surf mon path = do
  -- TODO: code duplication
  let x = int2Double $ fromEnum $ xrr_moninf_x mon
      y = int2Double $ fromEnum $ xrr_moninf_y mon
      w = fromEnum $ xrr_moninf_width  mon
      h = fromEnum $ xrr_moninf_height mon
  tmpSurf <- createSimilarSurface surf ContentColor w h
  renderWith tmpSurf (drawFromSurf surf x y)

  -- save as png and finish surf
  surfaceWriteToPNG tmpSurf path
  surfaceFinish tmpSurf


-- |Renders a region of a surface starting at the specified origin.
-- Used in 'saveMonitor'.
drawFromSurf :: Surface         -- ^surface to render from
             -> Double          -- ^X origin of the specified source
             -> Double          -- ^Y origin of the specified source
             -> Render ()
drawFromSurf rootSurf x y = do
  -- coordinate transform necessary to be able to specify the coordinates on
  -- the source surface, rather than the target surface
  setSourceSurface rootSurf (-x) (-y)
  paintWithAlpha 1.0
