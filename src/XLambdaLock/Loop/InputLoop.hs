-- |

module XLambdaLock.Loop.InputLoop
  ( runInputLoop
  ) where

import Colog

import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TChan
import Control.Monad (unless)
import Control.Monad.State (StateT(..), liftIO, evalStateT, modify, gets)
import Control.Monad.Trans.Class (lift)

import qualified Data.ByteString as BS
import qualified Data.Text as Text
import qualified Data.Text.IO as TextIO
import Data.Text.Encoding (encodeUtf8)
import Data.Time.Clock (getCurrentTime)
import Data.Time.Format (formatTime, defaultTimeLocale, rfc822DateFormat)

import System.IO
import System.Process
import System.Exit

import XLambdaLock.Auth.Handler
import XLambdaLock.Loop.Types
import XLambdaLock.Types


type InputLoopIO = StateT InputLoopData App

type Password = Text.Text

data InputLoopData = InputLoopData { channel  :: EventChan
                                   , password :: Password
                                   , authProc :: !ProcTuple
                                   }


runInputLoop :: EventChan -> App ()
runInputLoop ch = do
  -- disable stdin buffering so that every pressed key is read immediately
  liftIO $ hSetBuffering stdin NoBuffering

  ap <- createAuthProcIO
  evalStateT inputLoop InputLoopData { channel = ch
                                     , password = Text.empty
                                     , authProc = ap
                                     }


inputLoop :: InputLoopIO ()
inputLoop = do
  ch <- gets channel
  event <- processChar
  liftIO $ atomically $ writeTChan ch event
  -- if the password test was successful, we can return and exit
  if (event == PwSuccess)
    then return ()
    else inputLoop


-- |Function handling the processing of a char from stdin.
-- Blocks until a char can be read.
-- When a char is read, it converts it into a 'PwEvent'.
-- Calls 'handleAuthProc' internally if the password is committed.
processChar :: InputLoopIO PwEvent
processChar = do
  c <- liftIO getChar
  pwOld <- gets password
  let (pw, op) = handlePwChar pwOld c
  case op of
    PwCommit -> do res <- if Text.compareLength pw 0 == GT    -- check if a password has been entered
                            then handleAuthProc pw
                            else return $ PwError PwEmpty
                   -- clear password in case of failure or an error and emit warning
                   maybe
                     (return ())
                     (\ err -> do t <- liftIO $ getCurrentTime
                                  lift $ logWarning $ "Failed login attempt - "
                                    <> Text.pack (formatTime defaultTimeLocale rfc822DateFormat t)
                                    <> " - "
                                    <> Text.pack (show err)
                                  updatePw Text.empty)
                     (errorFromPwEvent res)
                   return res

    _ -> do updatePw pw
            return $ PwAction op (Text.length pw)


-- * Checking a password
-- |Contacts the APM saved in 'LoopData'.
-- Checks the password and returns the result.
-- Also handles restarting of the process in case of exit or crash.
handleAuthProc :: Password -> InputLoopIO PwEvent
handleAuthProc = handleAuthProc' False


-- |Wrapped by 'handleAuthProc'.
handleAuthProc' :: Bool                 -- ^Should always be instanciated with 'False'.
                                        -- This holds the information if the function has encountered
                                        -- a pw check before when it loops. If that is the case,
                                        -- this means that the password attempt has failed.
                -> Password
                -> InputLoopIO PwEvent
handleAuthProc' beenHere pw = do
  ap <- gets authProc
  let (authIn, authOut, authErr, p) = ap
  -- Check if the handle is closed. If it is, that is the same as an EOF,
  -- since we need to check the exit code and clean up in both cases.
  closed <- liftIO $ hIsClosed authOut
  eof <- if closed
           then return True
           else liftIO $ hIsEOF authOut
  if eof
    then do exitCode <- liftIO $ getProcessExitCode p
            liftIO $ cleanupProcess (Just authIn, Just authOut, Just authErr, p)
            unless (exitCode == Just ExitSuccess) updateProc  -- create new process
            lift $ logDebug $ "Got exit code: " <> Text.pack(show exitCode)
            return $ case exitCode of
              Just (ExitFailure     1) -> PwError PwFailure
              Just (ExitFailure (-13)) -> PwError PwLockout
              Just (ExitFailure     i) -> PwError (PwAuthError i)
              Just ExitSuccess         -> PwSuccess
              Nothing                  -> PwError (PwAuthError (-1))
    else do c <- liftIO $ hLookAhead authOut
            case (c, beenHere) of
              -- password
              ('P', True) -> return $ PwError PwFailure
              ('P', False) -> do _ <- liftIO $ getAuthMsg authOut
                                 liftIO $ ( do let bs = encodeUtf8 pw
                                               hPutStrLn authIn ('p':' ': (show . BS.length) bs)
                                               BS.hPut authIn $ bs `BS.snoc` 0x0a  -- append newline
                                          )
                                 handleAuthProc' True pw
              -- error
              ('e', _) -> do msg <- liftIO $ getAuthMsg authOut
                             lift $ logInfo $ "Error: " <> msg
                             return $ PwError (PwAuthError (-2))
              -- info
              ('i', _) -> do msg <- liftIO $ getAuthMsg authOut
                             lift $ logInfo $ "Info: " <> msg
                             -- do not update beenHere, since we have not read a password.
                             handleAuthProc' beenHere pw
              -- anything unhandled
              (ch, _)  -> do msg <- liftIO $ getAuthMsg authOut
                             lift $ logInfo $ "Unknown packet: " <> Text.singleton ch <> " : " <> msg
                             return $ PwError PwFailure


-- * Handling input
-- |Decides what to do with a new char. Returns the updated password, its length and wether it should be committed (tested).
handlePwChar :: Password -> Char -> (Password, PwOperation)
handlePwChar tup c =
  let operation = case c of
        '\DEL' -> PwDelete      -- backspace
        '\b'   -> PwDelete      -- backspace that does not delete a char. It does here anyway.

        '\ESC' -> PwReset       -- ESC to reset everything
        '\ETX' -> PwReset       -- C-c
        '\EOT' -> PwReset       -- C-d
        '\n'   -> PwCommit      -- RET to commit password
        _ -> PwAppend c
  in (handlePw operation tup, operation)


-- |Backend helper function processing a char.
handlePw :: PwOperation -> Password -> Password
handlePw (PwAppend c) pw = if Text.compareLength pw maxPwLen == LT
                                then pw `Text.snoc` c
                                else handlePw PwReset pw
handlePw PwDelete pw     = maybe Text.empty fst (Text.unsnoc pw)
handlePw PwReset  _      = Text.empty
handlePw PwCommit pw     = pw


-- * Helpers
-- |Helper to update the 'password'.
updatePw :: Password -> InputLoopIO ()
updatePw p = modify (\ state -> state { password = p })

-- |Helper to update the 'authProc'.
updateProc :: InputLoopIO ()
updateProc = do p <- lift $ createAuthProcIO
                modify (\ state -> state { authProc = p })
                lift $ logDebug "restarted authentication process"

-- |Helper to obtain the messages from the APM:
-- read one line and discard, return the content of the second line
getAuthMsg :: Handle -> IO Text.Text
getAuthMsg h = (liftIO . TextIO.hGetLine) h >> (liftIO . TextIO.hGetLine) h
