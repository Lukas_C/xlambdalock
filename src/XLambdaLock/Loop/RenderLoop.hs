{-# LANGUAGE NamedFieldPuns #-}
-- |

module XLambdaLock.Loop.RenderLoop
  ( runRenderLoop
  ) where

import Colog

import Control.Concurrent (threadDelay)
import Control.Concurrent.STM (atomically)
import Control.Concurrent.STM.TChan
import Control.Monad.State (StateT(..), evalStateT, gets, modify)
import Control.Monad.Trans.Class (lift)

import Data.Sequence (Seq(..))
import Data.Time.Clock

import Graphics.Rendering.Cairo
import Graphics.X11.Xlib (flush)
import Graphics.X11.Types

import XLambdaLock.Graphics.Window
import XLambdaLock.Loop.Types
import XLambdaLock.Types


type RenderLoopIO = StateT RenderLoopData App

-- |A sequence of 'Events' that need to be processed
type EventQueue = Seq Event

-- |Something that needs to be processed
data Event = LoopBlocker                -- ^Blocks loop for a certain amount of time
           | Event EventLambda UTCTime  -- ^Contains the function created by the 'EventHook'.
                                        -- Also contains the time of creation of the event, for animations and the like.

data RenderLoopData = RenderLoopData { channel :: EventChan
                                     , queue   :: !EventQueue
                                     , wProps  :: !WProps
                                     -- TODO: entire config or just the hooks?
                                     , config  :: !Config
                                     }

runRenderLoop :: EventChan
              -> XID
              -> Config
              -> App ()
runRenderLoop channel wid config = do
  -- initialize the drawing window
  wp <- liftIO $ initWindow wid
  let surf = wp_surf wp
      (w, h) = wp_dims wp
  -- run setupHook
  liftIO $ renderWith surf ((setupHook config) surf w h)

  evalStateT renderLoop RenderLoopData { channel
                                       -- initialization with a blocker will cause 'syncDpy' to be immediately called, flushing the setupHook
                                       , queue = Empty :|> LoopBlocker
                                       , wProps = wp
                                       , config
                                       }


renderLoop :: RenderLoopIO ()
renderLoop = do
  ch <- gets channel
  event <- liftIO $ atomically $ tryReadTChan ch
  q <- gets queue
  case event of
    Just e -> do eventToQ q e
                 -- skip processing, append everything from the queue first.
                 -- otherwise only ever one char will get added to the queue,
                 -- resulting in only one animation playing at a time.
                 renderLoop
    _ -> return ()

  processEventFromQueue q

  renderLoop


-- |Process an 'Event' from the 'EventQueue'.
processEventFromQueue :: EventQueue -> RenderLoopIO ()
processEventFromQueue Empty = do
  -- block until new event becomes available
  ch <- gets channel
  event <- liftIO $ atomically $ readTChan ch
  -- insert a new LoopBlocker along with the event
  eventToQ (Empty :|> LoopBlocker) event
processEventFromQueue (Event el creationTime :<| rest) = do
  wp <- gets wProps
  currTime <- liftIO $ getCurrentTime
  -- run EventLambda
  let surf = wp_surf wp
      (w, h) = wp_dims wp
      -- HACK: direct conversion would be better
      delta = fromRational $ toRational $ nominalDiffTimeToSeconds $ diffUTCTime currTime creationTime
  finished <- liftIO $ renderWith surf (el surf w h delta)
  if finished
    then updateQ $ rest
    else updateQ $ rest :|> Event el creationTime
-- if just the LoopBlock is remaining, remove it
processEventFromQueue (LoopBlocker :<| Empty) = do
  -- NOTE: flush here fixes missing feedback on incorrect password attempt
  syncDpy
  updateQ Empty
-- otherwise, sleep for the time specified and then append it again at the end
processEventFromQueue (LoopBlocker :<| rest) = do
  syncDpy
  -- HACK: This should also take processing delays into account by sleeping as long as needed to read 16ms (60Hz).
  liftIO $ threadDelay (1 * 16666)
  updateQ $ rest :|> LoopBlocker


-- |Helper function to update the 'ld_queue'.
updateQ :: EventQueue -> RenderLoopIO ()
updateQ q = modify (\ state -> state { queue = q })

-- |Create a new event using the current time for the timestamp
eventToQ :: EventQueue -> PwEvent -> RenderLoopIO ()
eventToQ q e = do
  c <- gets config
  time <- liftIO $ getCurrentTime
  updateQ (q :|> Event (eventHook c $ e) time)
  lift $ logDebug $ "got event"


syncDpy :: RenderLoopIO ()
syncDpy = do
  wp <- gets wProps
  let dpy = wp_display wp
      surf = wp_surf wp
      (w, h) = wp_dims wp
  liftIO $ flush dpy
  -- run loopHook
  c <- gets config
  liftIO $ renderWith surf ((loopHook c) surf w h)
