-- |

module XLambdaLock.Loop.Types where

import Control.Concurrent.STM.TChan

import XLambdaLock.Types

-- |Communication channel used for communication by the 'RenderLoop' and the 'InputLoop'
type EventChan = TChan PwEvent
