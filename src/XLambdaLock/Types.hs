{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE InstanceSigs #-}
-- |

module XLambdaLock.Types where

import Colog

import Control.Monad.Trans.Reader (ReaderT, runReaderT)
import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Reader.Class (MonadReader)

import Data.Maybe (isJust)

import Graphics.Rendering.Cairo (Surface, Render)

import System.Environment (lookupEnv)


-- * Config
-- |Function that is executed one time before the process replaces itself with xsecurelock.
-- This can be used e.g. for taking a screenshot of the desktop to set as a wallpaper later.
type InitHook = IO ()

-- |Function that is executed one time before the main loop is launched.
-- This can be used, e.g. for setting a background, do single time drawing and other stuff.
-- 'WProps' can be used to access parameters of the drawing window, like width and height.
type SetupHook = Surface        -- ^Root surface
               -> Double        -- ^Surface width
               -> Double        -- ^Surface height
               -> Render ()

-- |Function that is executed after the render loop processed all events once and the output buffer has been flushed.
-- This can be useful if some part of the Background needs to be redrawn because multiple 'EventLambda's paint with transparency.
-- In this case the the transparency effects of the 'EventLambda's should combine within one cycle of the render loop,
-- but the stack up over multiple render loop iterations is undesired.
-- See the 'XLambdaLock.Defaults.Defaults' implementation as an example.
type LoopHook = Surface         -- ^Root surface
              -> Double         -- ^Surface width
              -> Double         -- ^Surface height
              -> Render ()

-- |Function that is run when a new event is created.
-- It should return an 'EventLambda', which is effectively a closure that gets put onto the render loop
-- and is called everytime when the screen is about to be updated.
type EventHook = PwEvent -> EventLambda

-- |Function/Closure that is executed when ever an event is processed by the event queue.
type EventLambda = Surface              -- ^Root surface
                 -> Double              -- ^Surface width
                 -> Double              -- ^Surface height
                 -> DeltaSeconds        -- ^Time delta since the creation of the event, in seconds
                 -> Render Bool         -- ^Return wether the event can be removed

-- |Holds the arguments passed to 'xlambdalock'
data Config = Config { environment :: [(String, String)]        -- ^Holds additional environment variables that will be set. (helpful for configuring xsecurelock)
                     , initHook    :: InitHook
                     , setupHook   :: SetupHook
                     , loopHook    :: LoopHook
                     , eventHook   :: EventHook
                     }


-- * Constants
-- |To prevent the list from getting too large and causing an OOM crash,
-- the password field length is limited to 'maxPwLen' chars.
-- If this limit is exceeded, the password will be reset.
maxPwLen :: Int
maxPwLen = 256


-- * Password
-- |Possible operations on a password.
data PwOperation = PwAppend Char        -- ^Append a char to the end.
                 | PwDelete             -- ^Remove one char from the end.
                 | PwReset              -- ^Clear the password field.
                 | PwCommit             -- ^Submit the password for checking if it is correct.
                 deriving (Show, Eq)

-- |Represents something that might happen with a password while it is entered or processed.
data PwEvent = PwSuccess                -- ^Password entered was correct.
             | PwError PwError          -- ^Password entered was unsuccessful.
             | PwAction PwOperation Int -- ^An action was performed on the password.
                                        -- The Int holds the current length of the password.
             deriving (Show, Eq)

-- |Extracts a potential 'PwError' from a 'PwEvent'.
errorFromPwEvent :: PwEvent -> Maybe PwError
errorFromPwEvent (PwError err) = Just err
errorFromPwEvent _ = Nothing

-- |Represents possible errors why the password attempt was not successful.
data PwError = PwFailure                -- ^Password entered was incorrect.
             | PwEmpty                  -- ^Password entered was empty.
             | PwLockout                -- ^Max number of attempts granted by PAM were reached.
             | PwAuthError Int          -- ^Something went wrong while communicating with the auth process.
             deriving (Show, Eq)


-- * Cairo
-- |Represents a RGB color triplet of 'Double' types, for use with 'cairo'.
type Color = (Double, Double, Double)
-- |Represents an alpha channel of the 'Double' type, for use with 'cairo'.
type Alpha = Double


-- * Plumbing
-- ** Main app stack
-- |'AppData' holds the global state of the application
data Env m = Env
  { env_logAction :: !(LogAction m Message)
  }

-- |Main app stack, which is a transformed IO Monad with logging capabilities provided by @co-log@.
newtype App a = App
  { unApp :: ReaderT (Env App) IO a
  } deriving newtype (Functor, Applicative, Monad, MonadIO, MonadReader (Env App), MonadFail)

instance HasLog (Env m) Message m where
  getLogAction :: Env m -> LogAction m Message
  getLogAction = env_logAction
  {-# INLINE getLogAction #-}

  setLogAction :: LogAction m Message -> Env m -> Env m
  setLogAction newLogAction env = env { env_logAction = newLogAction }
  {-# INLINE setLogAction #-}


-- |Run the 'App'. Generates an environment automatically.
-- Logs at elevated level if the @DEBUG@ environment variable is set.
runAppEnv :: App a -> IO a
runAppEnv app = do
  debug <- lookupEnv "DEBUG"
  let logAction = cfilter
        -- log only if higher than debug or if debug option is explicitly enabled
        (\ (Msg sev _ _) -> or [(sev > Debug), isJust debug])
        (cmap fmtMessage logTextStdout)
  runApp app Env { env_logAction = logAction }

-- |Run the 'App' with a custom environment.
runApp :: App a -> Env App -> IO a
runApp app env = do
  runReaderT (unApp app) env


-- ** Helper Types
type DeltaSeconds = Double
